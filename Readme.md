#PiegOS
-----

##Présentation

> **PiegOS** est un projet open-source visant à comprendre le fonctionnement d'un noyaux rudimentaire.

##Programmers Zone

> Langages utilisés: nasm, C, AT&T ...

##Utilitaires requis pour la compilation

> Shell Unix (avec dd, cp etc...)<br />
> Make <br />
> GCC, ld etc...<br/>
> Nasm

##Comment utilisé le Noyaux Générer ?

> Un fichier PiegOS est créer à la racine du projet aprés l'éxécution de make. Il n'y a plus qu'à charger ce fichier à l'aide d'un chargeur d'amorçage (Grub, LILO etc..).

##Notes

> Le bootloader n'est présent qu'à titre informatif.<br />
> Pour le générer, lancez simple make dans le dossier bootloader. Un fichier bootloader.bin est alors générer dans ce même dossier.<br />
> Pour le lancer, il suffit de le placer sur le premier secteur du support à amorcer.

##Suite en construction...
  
