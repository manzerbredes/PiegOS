#include "gdt.h"
#include "../Helpers/types.h"
#include "../Helpers/memory.h"

//Declare global vars
GdtDescriptorStruct GDT_DESCRIPTORS[4];
GdtPointerStruct GDT_POINTER;

//Adapt parameter to the gdt descriptor structure
void gdt_initGdtDesc(u32 base, u32 limit, u8 access, u8 flags, GdtDescriptorStruct *Descriptor){
	Descriptor->limit1 = limit & 0xFFFF;
	Descriptor->base1 = base & 0xFFFF;
	Descriptor->base2 = (base & 0xFF0000) >> 16;
	Descriptor->access = access;
	Descriptor->limit2 = (limit & 0xF0000 ) >> 16;
	Descriptor->flags = flags & 0xFF;
	Descriptor->base3 = (base & 0xFF000000) >> 24;
}

//Copy the gdt into mémory and load it
void gdt_loadGdt(){

    //Init default segment
    gdt_initGdtDesc(0x0,0x0,0x0,0x0, &GDT_DESCRIPTORS[0]);

    //Init code segment
    gdt_initGdtDesc(0x0,0xFFFFF,0x9A,0x0D, &GDT_DESCRIPTORS[1]);

    //Init data segment
    gdt_initGdtDesc(0x0,0xFFFFF,0x92,0x0D, &GDT_DESCRIPTORS[2]);

    //Init stack segment
    gdt_initGdtDesc(0x00B00000,0x00000500,0x96,0x0D, &GDT_DESCRIPTORS[3]);


    //Init GDT Pointer
    GDT_POINTER.size=4*sizeof(GDT_DESCRIPTORS);
    GDT_POINTER.segment=0x00007E00;

	//Copy Gdt into memory and init registers
	memcpy((u32)GDT_DESCRIPTORS, (u32)GDT_POINTER.segment, (u32)GDT_POINTER.size);

    //Load the GDT
    __asm__("lgdtl (%0);"
            :
            :"r"(&GDT_POINTER)
    );
}


