#ifndef __GDT__
#define __GDT__

#include "../Helpers/types.h"


//Define GDT pointer
struct GdtPointerStruct{
	u16 size;
	u32 segment;
} __attribute__ ((packed));


//Define GDT descriptor
struct GdtDescriptorStruct{
	u16 limit1;
	u16 base1;
	u8 base2;
	u8 access;
	u8 limit2 : 4;
	u8 flags : 4;
	u8 base3;
} __attribute__ ((packed));


//Typedef :
typedef struct GdtPointerStruct GdtPointerStruct;
typedef struct GdtDescriptorStruct GdtDescriptorStruct;


//Functions :
void gdt_initGdtDesc(u32 base, u32 limit, u8 access, u8 flags, GdtDescriptorStruct *Descriptor);
void gdt_loadGdt();



#endif
