[BITS 32]

;Define extern symbol
extern _boot

;Define entry point as global for linking
global _kernelEntry

;Define kernel entry point
_kernelEntry:
	jmp begin ;Go to begin (for skip Multiboot Specification Header)


;----- Multiboot Specification Header -----
align 4	;Align data for 32 bits
dd 0x1BADB002
dd 0x0
dd 0x1BADB002
;----- End -----


;Run kernel
begin:
	call _boot ;Start kernel
