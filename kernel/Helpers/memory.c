#include "./memory.h"
#include "./types.h"

//Fonction to copy data into memory
int memcpy(u32 source, u32 dest, u32 size){

    //Init source and destination pointer
	u32 *sourceTmp=(u32 *)source;
	u32 *destTmp=(u32 *)dest;

    //Init progression
	u32 progress=0;

    //Start copy
	while(progress != size){

        //Copy
		*destTmp=*sourceTmp;

        //Update source and destination
		sourceTmp++;
		destTmp++;

        //Update progression
		progress++;
	}

    //End and return progression
	return progress;

}
