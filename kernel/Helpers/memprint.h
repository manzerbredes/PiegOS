#ifndef __memprint__
#define __memprint__

#include "./types.h"


#define MEMPRINTSTARTADR 0xB8000
#define MAXCURSORX 80
#define MAXCURSORY 25


//Define the bios color
enum colorBios{

    BLACK=0x0,
    BLUE=0x1,
    GREEN=0x2,
    CYAN=0x3,
    RED=0x4,
    MAGENTA=0x5,
    BROWN=0x6,
    LIGHTGRAY=0x7,
    DARKGRAY=0x8,
    LIGHTBLUE=0x9,
    LIGHTGREEN=0xA,
    LIGHTCYAN=0xB,
    LIGHTRED=0xC,
    LIGHTMAGENTA=0xD,
    YELLOW=0xE,
    WHITE=0xF

};

//Type def for biosColor
typedef enum colorBios colorBios;


//Update cursor position
void memprint_updateCursor();

//Set color
void memprint_setBackground(colorBios color);
void memprint_setForeground(colorBios color);

//Putchar
void memprint_putChar(u8 character);

//Print
void memprint_print(char *str);

//Scroll up
void memprint_scrollUp(u8 number);

//Clear screen
void memprint_clear();



#endif
