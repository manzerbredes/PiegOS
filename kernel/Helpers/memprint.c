#include "./memprint.h"

//Define global vars
u8 MEMPRINT_CURSORX=0;
u8 MEMPRINT_CURSORY=0;
u8 MEMPRINT_COLORS=0x0F;



//Move cursor
void memprint_updateCursor(){


    //Update X axis
    MEMPRINT_CURSORX++;

    //Check X value
    if(MEMPRINT_CURSORX >= MAXCURSORX){

        //If X is out of the screen
        MEMPRINT_CURSORX=0;

        //Update Y
        MEMPRINT_CURSORY++;

        //Check Y value
        if(MEMPRINT_CURSORY > MAXCURSORY){

            //If Y is out of the screen
            memprint_scrollUp(1);

            //Decrease Y value
            MEMPRINT_CURSORY--;
        }


    }


}

//Change character background color
void memprint_setBackground(colorBios color){
    u8 newColor= (color << 4);
    MEMPRINT_COLORS= newColor | ((MEMPRINT_COLORS << 4) >> 4);
}

//Change character color
void memprint_setForeground(colorBios color){
    u8 newColor= color;
    MEMPRINT_COLORS= newColor | ((MEMPRINT_COLORS >> 4) << 4);
}

//Print a char
void memprint_putChar(u8 character){

    //Get the adresse with the cursor position
    char *adress= ((char *) MEMPRINTSTARTADR) + (MEMPRINT_CURSORX * 2) + (MEMPRINT_CURSORY * MAXCURSORX * 2);

    //Copy the character
    *adress=character;

    //Copy his attribute
    adress++;
    *adress=MEMPRINT_COLORS;

    //Update cursor position
    memprint_updateCursor();

}


//Print a char*
void memprint_print(char *str){
    while(*str!=0x0){
        memprint_putChar(*str);
        str++;
    }
}

//Clear the screen
void memprint_clear(){
    memprint_scrollUp(MAXCURSORY);
}

//Scroll up "number" times
void memprint_scrollUp(u8 number){

    //Get number of adress (char & his attribute) to scroll
    int nbAdrToScroll=number*MAXCURSORX*2;

    int i=0;
    //Scroll all of the characters and attributes
    for(i;i!=MAXCURSORX*2*MAXCURSORY;i++){

        //Get source character or attribute
        char* source=(((char *)MEMPRINTSTARTADR) + i);

        //Get destination character or attribute
        char* dest=source-nbAdrToScroll;

        //Check if destination is out of the screen
        if(dest >= (char *)MEMPRINTSTARTADR)
            *dest=*source;

        //Remove data from source
        *source=0x0;

    }

}
