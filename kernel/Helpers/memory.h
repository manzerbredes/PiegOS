#ifndef __memory__
#define __memory__

#include "./types.h"

//Fonction to copy data into memory
int memcpy(u32 source, u32 dest, u32 size);


#endif
