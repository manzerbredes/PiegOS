//To load GDT
#include "GDT/gdt.h"
#include "./Helpers/memprint.h"


//----- PiegOS kernel main -----
int  main(){

    //Welcome
    memprint_print("Welcome to PiegOS");

    //Infinite loop
	while(1);

    //Exit code
	return 0;
}


//----- PiegOS kernel boot function -----
void _boot(){

    //Load GDT
    gdt_loadGdt();

    //Init all segments and stack
    __asm__("\
        movw $0x10, %ax; \n \
        movw %ax, %ds; \n \
        movw %ax, %es \n \
        ljmp $0x08, $updateDS;\
        updateDS: \n\
        movw $0x18, %ax \n \
        movw %ax, %ss \n \
        movl $0x00B00000, %esp \n\
    ");

    //Call main function after stack pointer changing (due to C optimisation)
    main();
}

