EXEC=PiegOS

all:$(EXEC)

#----- Copy kernel -----
$(EXEC): kernel/kernel.bin
	cp kernel/kernel.bin ./$(EXEC)
#-----------------------


#----- Gen Kernel -----
kernel/kernel.bin:
	cd kernel/ && make
#----------------------


#----- Other -----
.PHONY: clean

clean:
	cd kernel && make clean
	-rm $(EXEC)
#-----------------
