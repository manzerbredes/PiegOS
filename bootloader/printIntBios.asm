printIntBios:

	;Save registers
	push ax
	push bx

	;Print loop
	.loop:

		mov ah, 0x0E	;Use 0xE bios service for teletype print
		lodsb		;Load char in al and inc SI register
		cmp al, 0x0	;Check if we print all the chain
		je .end		;If yes go to end

		mov bl, 0x0F	;Else set color register

		int 0x10	;And print the character

		jmp .loop	;Go to the next character
	.end:

	;Restore registers
	pop bx
	pop ax

	;Back to previous task
	ret
