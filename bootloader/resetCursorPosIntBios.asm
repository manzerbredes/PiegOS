resetCursorPosIntBios:

	;Save registers
	push ax
	push bx
	push dx

	mov ax, 0x0200  ;Move cursor interrupt
	mov bx, 0x0000	; X position
	mov dx, 0x0000	; Y Position

	;Call 0x10 interrupt service
	int 0x10

	;Restore registers
	pop dx
	pop bx
	pop ax

	;Back to previous task
	ret
