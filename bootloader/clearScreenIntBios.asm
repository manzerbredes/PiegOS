clearScreenIntBios:

	;Save registers
	push ax
	push bx
	push cx
	push dx

	mov ax, 0x0600 ;Clear
	mov bx, 0x0F00 ;Color black behind white foreground

	mov cx, 0x0000 ;Top left corner
	mov dx, 0x5050 ;Bottom right corner 80x80

	int 0x10 ;Clear the screen

	;Restore registers
	pop dx
	pop cx
	pop bx
	pop ax

	;Reset Cursor Position
	call resetCursorPosIntBios

	;Back to previous task
	ret

;Include resetCursorPosIntBios
%include "resetCursorPosIntBios.asm"
