;Start 16 BITS bootloader program
[BITS 16]

;Save the first adress with a label to complete the MBR at the end.
start:

;Include bios routines and jump to skip including code
jmp skipInc
%include "clearScreenIntBios.asm"
%include "printIntBios.asm"
skipInc:

;Init CPU registers
mov ax, 0x07C0	;Put bootloader adress in ax register
mov ds, ax	;Init data segment

;Init stack from 0x80000 to 0x8f000
mov ax, 0x8000
mov ss, ax	;Set stack segment
mov ax, 0x0f00
mov sp, ax	;Set stack offset

;Clear the screen
call clearScreenIntBios

;Print msg
mov si, helloBootloader ;load msg in si register
call printIntBios	;print the msg

;Pause here !
infiniteLoop:
	jmp infiniteLoop

;Define data
helloBootloader db "PiegOS bootloader successfully running !", 0

;Complete the MBR with nothing
times 510 - ($ - start) db 0x0

;Declare magic number
dw 0xAA55
